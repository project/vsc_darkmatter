<?php
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language; ?>" xml:lang="<?php print $language->language; ?>">
<head>
<title><?php print $head_title; ?></title>
<?php print $head; ?>
<?php print $styles; ?>
<?php print $scripts; ?>
</head>

<body <?php print $body_classes; ?>>
  <?php if (!empty($admin)) print $admin; ?>
  <div id="page" <?php print $page_classes; ?>>
   <div id="page-inner" class="inner">
     <?php print $content; ?>
     <?php print $closure; ?>
   </div> <!-- /page-inner -->
  </div> <!-- /page -->

</body>
</html>